#ifndef LINKEDLIST_NODE_H
#define LINKEDLIST_NODE_H

template<typename T>
class Node {
public:
    T data;
    Node *next;

    Node(T _data, Node *_next);
};


#endif //LINKEDLIST_NODE_H
