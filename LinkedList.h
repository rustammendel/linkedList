#ifndef LINKEDLIST_LINKEDLIST_H
#define LINKEDLIST_LINKEDLIST_H

#include "Node.h"

using namespace std;

enum exceptions {
    DuplicateNode, NullReferenceInLLWithHEAD
};

template<typename T>
class LinkedList {

private:
    Node<T> *HEAD;

    ///Find and return reference to the node which is less than _qnode
    Node<T> *findRefL(Node<T> *_qnode);

    ///Insert node to linked list at given ref
    void insert(Node<T> *ref, Node<T> *_newnode);

    ///Find and remove _node in sorted (??????) LL
    void rem(Node<T> *_node);


public:

    LinkedList();

    ///Getter for HEAD
    Node<T> *getHEAD();

    ///Add given node to sorted LL
    void add(Node<T> *_node);

    ///Insert _newnode to sorted LL with given HEAD pointer
    void sortedInsert(Node<T> *_newnode);

    ///Delete given node from sorted LL
    void del(Node<T> *_delNode);

    ///Print whole LL with predefined HEAD pointer
    void dump();
};


#endif //LINKEDLIST_LINKEDLIST_H
