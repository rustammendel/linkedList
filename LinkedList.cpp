#include <cstdio>
#include "LinkedList.h"
#include "Node.cpp"

template<typename T>
Node<T> *LinkedList<T>::findRefL(Node<T> *_qnode) {

    Node<T> *ref;
    if (HEAD == nullptr) {
        ref = nullptr;
        printf("HEAD is null, nullptr returned\n");

    } else if ((HEAD)->data >= _qnode->data) {
        ref = nullptr;

    } else {
        ref = HEAD;
        while (ref->next != nullptr &&
               ref->next->data < _qnode->data) {
            ref = ref->next;

        }
    }

    return ref;
}

template<typename T>
void LinkedList<T>::insert(Node<T> *ref, Node<T> *_newnode) {
    if (ref == nullptr) { //HEAD is null
        if (HEAD == nullptr)
            HEAD = _newnode;
        else
            throw NullReferenceInLLWithHEAD;
    } else {
        _newnode->next = (ref)->next;
        (ref)->next = _newnode;
    }
}

template<typename T>
void LinkedList<T>::sortedInsert(Node<T> *_newnode) {

    Node<T> *current = findRefL(_newnode);
    if (current != nullptr && _newnode == current->next) {
        throw DuplicateNode;

    } else if (current == nullptr && HEAD != nullptr) { //HEAD bigger than newnode
        _newnode->next = HEAD;
        (HEAD) = _newnode;

    } else
        insert(current, _newnode);

}

template<typename T>
void LinkedList<T>::rem(Node<T> *_node) {

    Node<T> *prev = findRefL(_node);
    if (prev != nullptr) {
        prev->next = prev->next->next;

    }
}

template<typename T>
LinkedList<T>::LinkedList() {
    HEAD = nullptr;

}

template<typename T>
Node<T> *LinkedList<T>::getHEAD() {
    return HEAD;

}

template<typename T>
void LinkedList<T>::add(Node<T> *_node) {

    try {
        sortedInsert(_node);
    } catch (exceptions) {
        printf("Duplicate Node will be ignored\n");

    }
}

template<typename T>
void LinkedList<T>::dump() {

    Node<T> *i = HEAD;
    while (i != nullptr) {
        printf("List: %d \n", (i)->data);
        i = (i)->next;

    }
}

template<typename T>
void LinkedList<T>::del(Node<T> *_delNode) {
    rem(_delNode);

}

