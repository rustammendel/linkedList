#include "LinkedList.h"
#include "LinkedList.cpp"


using namespace std;

int main() {

    Node<int> one = Node<int>(1, nullptr);
    Node<int> two = Node<int>(2, nullptr);
    Node<int> three = Node<int>(3, nullptr);
    Node<int> threend = Node<int>(3, nullptr);
    Node<int> four = Node<int>(4, nullptr);

    LinkedList<int> ll = LinkedList<int>();

    ll.add(&three);
    ll.add(&one);
    ll.add(&two);
    ll.add(&four);
    ll.add(&threend);

    ll.del(&threend);

    ll.dump();


    return 0;
}